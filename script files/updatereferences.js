var projectFile = "C:\\DataST\\Live\\CRXIT_UK_DEV_RESIPUK\\Latest Source\\32_Development\\SourceCode\\RESIPServices\\RESIP.Services.BL\\RESIP.Services.BL.csproj";
var binDir ="C:\\DataST\\Live\\CRXIT_UK_DEV_HDS\\Latest Source\\32_Development\\SourceCode\\TempArtifacts\\Binaries\\";

if(WScript.Arguments.Unnamed.Count < 2)
{
	WScript.Echo("Usage: updateReference <Project file> <binFolder>");
	WScript.Quit(1); // return with error
}

projectFile = WScript.Arguments.Unnamed.Item(0);
binDir = WScript.Arguments.Unnamed.Item(1);


var doc = new ActiveXObject("msxml2.domdocument");

doc.load(projectFile);

var nodes = doc.selectNodes("Project/ItemGroup/ProjectReference");

for(var i =0;i<nodes.length;i++)
{
	//Process the node and then remove it.
	AddReference(doc,nodes[i]);
	nodes[i].parentNode.removeChild(nodes[i]);
}
try
{
    doc.save(projectFile);
}
catch(e)
{
    //Access denied.
}


function AddReference(doc, node)
{
	//Given a project reference node create a dll reference instead.
    var sProject = new String();    
    var a = node.selectSingleNode("@Include");
    sProject = a.nodeTypedValue;
    arSplit = sProject.split("\\");
    sProject = arSplit[arSplit.length -1].substring(0, arSplit[arSplit.length -1].length - 7); ;
    
    var refNode = doc.selectSingleNode("Project/ItemGroup[Reference]");
    
    
    var newNode = AddXMLNode(doc,refNode,"Reference","");
    
    var attrib =  doc.createAttribute("Include");
    //RESIP.Framework, Version=1.0.0.0, Culture=neutral, processorArchitecture=MSIL
    attrib.nodeTypedValue =sProject +", Version=1.0.0.0, Culture=neutral, processorArchitecture=MSIL";
    newNode.setAttributeNode(attrib);
    
    AddXMLNode(doc,newNode,"SpecificVersion","False");
    AddXMLNode(doc,newNode,"HintPath",binDir + sProject + ".dll");
    
    
}

function AddXMLNode(mainDoc,parent,name,value)
{
    var newNode = mainDoc.createNode(1,name,"http://schemas.microsoft.com/developer/msbuild/2003");
    if(value.length !=0)
    {
        newNode.nodeTypedValue = value;
    }
    parent.appendChild(newNode);
    
    return newNode;
    
}