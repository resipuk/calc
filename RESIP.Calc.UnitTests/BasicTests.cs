﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using RESIP.Calc.BL;

namespace RESIP.Calc.UnitTests
{
   [TestFixture]

    public class BasicTests
    {
       [Test]
       public void AddTests()
       {
           Operation op = new Operation();
           Assert.AreEqual(1, op.Add(0, 1).Value, "0 add 1");

           Assert.AreEqual(-1, op.Add(0, -1).Value, "0 add -1");
           Assert.AreEqual(0, op.Add(0, 0).Value, "0 add 0"); 
       }

       [Test]
       public void SubtractTests()
       {
           Operation op = new Operation();
           Assert.AreEqual(-1, op.Subtract(0, 1).Value, "0 subtract 1");

           Assert.AreEqual(1, op.Subtract(0, -1).Value, "0 subtract -1");
           Assert.AreEqual(0, op.Subtract(0, 0).Value, "0 subtract 0");
       }

       [Test]
       public void MultiplyTests()
       {
           Operation op = new Operation();
           Assert.AreEqual(0, op.Multiply(0, 1).Value, "1 multiply 0");
           Assert.AreEqual(0, op.Multiply(0, -1).Value, "0 multiply -1");
           Assert.AreEqual(-1, op.Multiply(-1, 1).Value, "-1 multiply 1");
       }

       [Test]
       public void DivideTests()
       {
           Operation op = new Operation();
           Assert.AreEqual(2.5, op.Divide(5, 2).Value, "5 divide 2");
           Assert.AreEqual(0, op.Divide(-1, 0).Value, "-1 divide 0");
           Assert.AreEqual(1, op.Divide(1, 1).Value, "1 divide 1");
       }

       [Test(Description="Test for using the power")]
       public void PowerTests()
       {
           Operation op = new Operation();
           Assert.AreEqual(4, op.Power(2, 2).Value, "2 to the power 2");
           Assert.AreEqual(16, op.Power(4, 2).Value, "4 to the power 2");
           Assert.AreEqual(36, op.Power(6, 2).Value, "6 to the power 2");
           
       }



       [Test]
       public void SquareRootTests()
       {
           Operation op = new Operation();
           Assert.AreEqual(8, op.SquareRoot (64).Value, "SquareRoot 64");
           Assert.AreEqual(7, op.SquareRoot(49).Value, "SquareRoot 49");
           Assert.AreEqual(6, op.SquareRoot(36).Value, "SquareRoot 36");
           Assert.AreEqual(5, op.SquareRoot(25).Value, "SquareRoot 25");
       }

       [Test]
       public void PITests()
       {
           Operation op = new Operation();
           Assert.AreEqual(Math.PI, op.PI().Value, "PI");
       }

       //Comment.  There is not sign of sin yet!
       [Test]
       public void TestTAN()
       {
           Operation op = new Operation();
           Assert.AreEqual(-2.1850398632615189, op.TAN(2).Value, "TAN 2");
               
       }

       [Test]
       public void TestCOS()
       {
           Operation op = new Operation();
           Assert.AreEqual(-0.82930983286315019, op.COS(54).Value, "COS 54");

       }

       [Test]
       public void TestSine()
       {
           Operation op = new Operation();
           Assert.AreEqual(-0.55878904885161629, op.Sine(54).Value, "Sine 54"); 

       }

       [Test]
       public void TestTanh()
       {
           Operation op = new Operation();
           Assert.AreEqual(1, op.TANH(54).Value, "TANh 54");

       }

   }
}
