﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RESIP.Calc.BL.Entities;
using RESIP.Calc.BL;
using Cegedim.Framework.Logging;

namespace RESIP.Calc.WebServices
{
    [LogTrace]
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Operations : IOperations
    {
        //Change 1
        public MessageResponse GetAdd(double v1, double v2)
        {
            Operation op = new Operation();
            return op.Add(v1, v2);
        }

        //sUBTRACT ONE FROM ANOTHER
        public MessageResponse GetSubtract(double v1, double v2)
        {
            Operation op = new Operation();
            return op.Subtract(v1, v2);
        }

        public MessageResponse GetMultiply(double v1, double v2)
        {
            Operation op = new Operation();
            return op.Multiply(v1, v2);
        }

        public MessageResponse GetDivide(double v1, double v2)
        {
            Operation op = new Operation();
            return op.Divide(v1, v2);
        }
        //Comment for demo

        public MessageResponse GetModulus(double v1, double v2)
        {
            Operation op = new Operation();
            return op.Modulus(v1, v2);
        }

        //Return the v1 Power 2
        public MessageResponse GetSquare(double v1)
        {
            Operation op = new Operation();
            return op.Square(v1);
        }




        public MessageResponse GetSquareRoot(double v1)
        {
            Operation op = new Operation();
            return op.SquareRoot(v1);
        }


        // Return the Power v2 to v2
        public MessageResponse GetPower(double v1, double v2)
        {
            Operation op = new Operation();
            return op.Power(v1, v2);
        }

        public MessageResponse GetPI()
        {
            Operation op = new Operation();
            return op.PI();
        }

        public MessageResponse GetTAN(double v1)
        {
            Operation op = new Operation();
            return op.TAN(v1);
        }

        public MessageResponse GetCOS(double v1)
        {
            Operation op = new Operation();
            return op.COS(v1);
        }

        public MessageResponse GetSINE(double v1)
        {
            Operation op = new Operation();
            return op.Sine(v1);
        }

        public MessageResponse GetTANH(double v1)
        {
            Operation op = new Operation();
            return op.TANH(v1);
        }
    }
}
