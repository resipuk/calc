﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using RESIP.Calc.BL.Entities;

namespace RESIP.Calc.WebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IOperations
    {

        [WebGet]
        MessageResponse GetAdd(double v1, double v2);

        [WebGet]
        MessageResponse GetSubtract(double v1, double v2);

        [WebGet]
        MessageResponse GetMultiply(double v1, double v2);

        [WebGet]
        MessageResponse GetDivide(double v1, double v2);

        [WebGet]
        MessageResponse GetModulus(double v1, double v2);

        [WebGet]
        MessageResponse GetSquare(double v1);

        [WebGet]
        MessageResponse GetSquareRoot(double v1);
        //Comment
        [WebGet]
        MessageResponse GetPower(double v1, double v2);

        [WebGet]
        MessageResponse GetPI();


        [WebGet]
        MessageResponse GetTAN(double v1);

        [WebGet]
        MessageResponse GetCOS(double v1);

        [WebGet]
        MessageResponse GetSINE(double v1);

        [WebGet]
        MessageResponse GetTANH(double v1);
    }


}
