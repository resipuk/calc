﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RESIP.Calc.BL.Entities;
using Cegedim.Framework.Logging;

namespace RESIP.Calc.BL
{ 
    [LogTrace]
    public class Operation
    {


        //Branch 1

        //Branch 2

        private const int Constant1 = 1;
        public const int TWO = 2;
        private const int Constant3 = 3;
        public const int FOUR = 4;
        private const int Constant5 = 5;
        public const int SIX= 6;

        public MessageResponse Add(double v1, double v2)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = v1 + v2;
            return msg;
        }

        public MessageResponse Subtract(double v1, double v2)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = (v1 - v2);
            return msg;
        }

        public MessageResponse Multiply(double v1, double v2)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = v1 * v2;
            return msg;
        }

        public MessageResponse Divide(double v1, double v2)
        {
            MessageResponse msg = new MessageResponse();
            if (v2 == 0)
            {
                msg.Value = 0;
            }
            else
            {
                msg.Value = v1 / v2;
            }
            return msg;
        }

        public MessageResponse Modulus(double v1, double v2)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = v1 % v2;
            return msg;


        }

        public MessageResponse Square(double v1)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = Math.Pow(v1, 2); 
            return msg;
        }


        public MessageResponse SquareRoot(double v1)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = Math.Sqrt(v1); 
            return msg;
        }


        public MessageResponse Power(double v1, double v2)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = Math.Pow(v1, v2); 
            return msg;
        }

        public MessageResponse PI()
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = Math.PI;
            return msg;
        }


        //Comment.  There is not sign of sin yet!
        public MessageResponse TAN(double v1)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = Math.Tan(v1);
            return msg;
        }

        public MessageResponse COS(double v1)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = Math.Cos(v1);
            return msg;
        }

        public MessageResponse Sine(double v1)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = Math.Sin(v1);
            return msg;
        }

        public MessageResponse TANH(double v1)
        {
            MessageResponse msg = new MessageResponse();
            msg.Value = Math.Tanh(v1);
            return msg;
        }
    }
}
